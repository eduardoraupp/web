-- used in tests that use HSQL
create table user (
	id BIGINT auto_increment PRIMARY KEY,
	username VARCHAR(256),
    password VARCHAR(256)
);

create table role (
	id BIGINT auto_increment PRIMARY KEY,
    role VARCHAR(256)
);

create table user_role (
	id_user BIGINT,
    id_role BIGINT,
    PRIMARY KEY(id_user, id_role)
);

insert into user values (1, 'admin', 'admin');
insert into role values (1, 'ADMIN');
insert into user_role values (1, 1);
insert into user values (2, 'page1', 'page1');
insert into role values (2, 'PAGE_1');
insert into user_role values (2, 2);
insert into user values (3, 'page3', 'page3');
insert into role values (3, 'PAGE_3');
insert into user_role values (3, 3);
insert into user values (4, 'page2', 'page2');
insert into role values (4, 'PAGE_2');
insert into user_role values (4, 4);