package com.javacore.infrastructure.filter;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javacore.configuration.HttpServerRuleTest;
import com.javacore.domain.user.Role;
import com.javacore.domain.user.User;

public class FrontControllerTest {

	@Rule
	public final HttpServerRuleTest httpServer = new HttpServerRuleTest();

	@Test
	public void testLogin() throws ClientProtocolException, IOException {
		final CredentialsProvider provider = new BasicCredentialsProvider();
		final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("admin", "admin");
		provider.setCredentials(AuthScope.ANY, credentials);
		final HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider)
				.build();
		final HttpPost post = new HttpPost("http://localhost:8080/login");
		final HttpResponse response = httpClient.execute(post);
		Assert.assertEquals(200, response.getStatusLine().getStatusCode());
	}

	@Test
	public void testAccessResourceWithWrongParameters() throws ClientProtocolException, IOException {
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("roles", "ADMIN");
		post.setHeader("username", "eduardo");
		final HttpResponse response = httpClient.execute(post);
		Assert.assertEquals(400, response.getStatusLine().getStatusCode());
	}

	@Test
	public void testAccessResourceWithAdminRole() throws ClientProtocolException, IOException {
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");
		final User user = new User();
		user.setPassword("abcde");
		user.setUsername("dudu");
		final Role role = new Role("ADMIN");
		user.addRole(role);
		final HttpEntity entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		final HttpResponse response = httpClient.execute(post);
		Assert.assertEquals(201, response.getStatusLine().getStatusCode());
	}

	@Test
	public void testAccessResourceWithoutRole() throws ClientProtocolException, IOException {
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		final HttpResponse response = httpClient.execute(post);
		Assert.assertEquals(401, response.getStatusLine().getStatusCode());
	}

	@Test
	public void testAccessResourceWithoutSession() throws ClientProtocolException, IOException {
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		final HttpResponse response = httpClient.execute(post);
		Assert.assertEquals(403, response.getStatusLine().getStatusCode());
	}

	@Test
	public void testAccessResourceExpiredCookie() throws ClientProtocolException, IOException {
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Sat, 08-Apr-2017 00:06:44 CEST");
		post.setHeader("roles", "ADMIN");
		post.setHeader("username", "admin");
		final HttpResponse response = httpClient.execute(post);
		Assert.assertEquals(403, response.getStatusLine().getStatusCode());
	}

}
