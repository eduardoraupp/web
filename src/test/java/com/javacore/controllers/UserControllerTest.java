package com.javacore.controllers;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javacore.configuration.HttpServerRuleTest;
import com.javacore.domain.user.Role;
import com.javacore.domain.user.User;
import com.javacore.domain.user.UserException;
import com.javacore.infrastructure.dao.RoleDAO;
import com.javacore.infrastructure.repositories.UserRepository;
import com.javacore.utils.StringUtils;

public class UserControllerTest {

	@Rule
	public final HttpServerRuleTest httpServer = new HttpServerRuleTest();
	private final UserRepository userRepository = new UserRepository();
	private final RoleDAO roleDAO = new RoleDAO();

	@Test
	public void saveUserTest() throws ClientProtocolException, IOException {
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");
		final User user = new User();
		user.setPassword("abcde");
		user.setUsername("edu");
		final Role role = new Role("ADMIN123");
		user.addRole(role);
		final HttpEntity entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		httpClient.execute(post);
		final User userCreated = this.userRepository.getUserWithRolesByUsernameAndPassword(user);
		Assert.assertTrue(userCreated != null);
	}

	@Test(expected=UserException.class)
	public void removeUserTest() throws ClientProtocolException, IOException {
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");
		final User user = new User();
		user.setPassword("abcde");
		user.setUsername("rauppp");
		final Role role = new Role("ADMIN123");
		user.addRole(role);
		final HttpEntity entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		httpClient.execute(post);
		final User userCreated = this.userRepository.getUserWithRolesByUsernameAndPassword(user);
		final HttpDelete delete = new HttpDelete("http://localhost:8080/users/".concat(String.valueOf(userCreated.getId())));
		delete.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		delete.setHeader("username", "eduardo");
		delete.setHeader("roles", "ADMIN");
		httpClient.execute(delete);
		this.userRepository.getUserWithRolesByUsernameAndPassword(user);
	}

	@Test
	public void updateUsername() throws ClientProtocolException, IOException {
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");
		final User user = new User();
		user.setPassword("abcde");
		user.setUsername("raupp2");
		final Role role = new Role("ADMIN123");
		user.addRole(role);
		HttpEntity entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		httpClient.execute(post);
		final User userCreated = this.userRepository.getUserWithRolesByUsernameAndPassword(user);
		final HttpPut put = new HttpPut("http://localhost:8080/users");
		put.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		put.setHeader("username", "eduardo");
		put.setHeader("roles", "ADMIN");
		userCreated.setUsername("eia");
		entity = new ByteArrayEntity(gson.toJson(userCreated).getBytes("UTF-8"));
		put.setEntity(entity);
		httpClient.execute(put);
		final User userUpdated = this.userRepository.getUserWithRolesByUsernameAndPassword(userCreated);
		Assert.assertTrue(userUpdated.getUsername().equals("eia"));
	}

	@Test
	public void updateUserRoles() throws ClientProtocolException, IOException {
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		final HttpClient httpClient = HttpClientBuilder.create().build();
		final HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");
		final User user = new User();
		user.setPassword("abcde");
		user.setUsername("raupp22");
		Role role = new Role("ADMIN12300");
		user.addRole(role);
		HttpEntity entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		httpClient.execute(post);
		final User userCreated = this.userRepository.getUserWithRolesByUsernameAndPassword(user);
		final HttpPut put = new HttpPut("http://localhost:8080/users");
		put.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		put.setHeader("username", "eduardo");
		put.setHeader("roles", "ADMIN");
		role = new Role("ADMIN12345123");
		this.roleDAO.save(role);
		userCreated.addRole(this.roleDAO.getRoleByName(role.getRole()));
		entity = new ByteArrayEntity(gson.toJson(userCreated).getBytes("UTF-8"));
		put.setEntity(entity);
		httpClient.execute(put);
		final User userUpdated = this.userRepository.getUserWithRolesByUsernameAndPassword(userCreated);
		Assert.assertTrue(userUpdated.getRoles().size() == 2);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUsersTest() throws ClientProtocolException, IOException {
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");

		User user = new User();
		user.setPassword("abcde");
		user.setUsername("edur");
		Role role = new Role("ADMIN123");
		user.addRole(role);
		HttpEntity entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		httpClient.execute(post);

		post = new HttpPost("http://localhost:8080/users");
		post.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		post.setHeader("username", "eduardo");
		post.setHeader("roles", "ADMIN");

		user = new User();
		user.setPassword("abcde");
		user.setUsername("edur2");
		role = new Role("ADMIN123dw3");
		user.addRole(role);
		entity = new ByteArrayEntity(gson.toJson(user).getBytes("UTF-8"));
		post.setEntity(entity);
		httpClient.execute(post);

		httpClient = HttpClientBuilder.create().build();
		final HttpGet get = new HttpGet("http://localhost:8080/users");
		get.setHeader("session", "Tue, 08-Apr-2025 00:06:44 CEST");
		get.setHeader("username", "eduardo");
		get.setHeader("roles", "ADMIN");
		final HttpResponse response = httpClient.execute(get);
		final String responseJson = StringUtils.getStringFromInputStream(response.getEntity().getContent());
		final List<User> users = gson.fromJson(responseJson, List.class);
		Assert.assertTrue(users.size() > 1);
	}


}
