package com.javacore.configuration;

import org.junit.rules.ExternalResource;

import com.sun.net.httpserver.HttpServer;

public class HttpServerRuleTest extends ExternalResource {

	private HttpServer server;

	@Override
	protected void before() throws Throwable {
		this.server = Server.startServer();
	}

	@Override
	protected void after() {
		if (this.server != null) {
			this.server.stop(0);
		}
	}

}
