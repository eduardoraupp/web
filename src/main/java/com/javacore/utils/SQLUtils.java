package com.javacore.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLUtils {

	public static void closeQueryIODB(final ResultSet rs, final PreparedStatement stmt) {
		try {
			stmt.close();
		} catch (final SQLException e1) {
			throw new IllegalStateException(e1);
		}
		try {
			rs.close();
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	public static void closeSessionDB(final Connection connection, final PreparedStatement stmt) {
		try {
			connection.commit();
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		}
		try {
			stmt.close();
		} catch (final SQLException e1) {
			throw new IllegalStateException(e1);
		}
	}

}
