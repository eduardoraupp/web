package com.javacore.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {

	public static String getStringFromInputStream(final InputStream is) throws IOException {
		final StringBuilder sb = new StringBuilder();
		String line;
		try (final BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (final IOException e) {
			throw e;
		}
		return sb.toString();
	}

	public static boolean containsPathParameter(final String[] parameters) {
		return parameters.length > 2;
	}

	public static String[] getPathParameters(final String[] parameters) {
		return StringUtils.containsPathParameter(parameters) ? Arrays.copyOfRange(parameters, 2, parameters.length)
				: null;
	}

	public static String cleanPathResource(final String address) {
		final String[] paths = address.split("/");
		return Arrays.asList(Arrays.copyOfRange(paths, 1, paths.length)).stream().map(path -> path.trim())
				.filter(path -> !path.contains(",") && !path.contains("{") && !path.contains("}"))
				.collect(Collectors.joining(", "));
	}

	public static boolean verifySomeEqualsGroup(final String userRoles, final List<String> endPointRoles) {
		final List<String> userRolesList = Arrays.asList(userRoles.split(","));
		return userRolesList.stream().filter(user -> endPointRoles.contains(user.trim())).findAny().isPresent();
	}

}
