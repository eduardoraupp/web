package com.javacore.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.javacore.infrastructure.http.CookieException;
import com.sun.net.httpserver.HttpExchange;

public class CookieUtils {

	private final static DateFormat df = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss zzz");
	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd-MMM-yyyy HH:mm:ss zzz");

	public static LocalDateTime getCookieDate(final HttpExchange http) {
		final String cookie = http.getRequestHeaders().getFirst("session");
		if(cookie == null) {
			throw new CookieException("You do not have a valid Cookie to access the wished resource");
		}
		return LocalDateTime.parse(cookie, CookieUtils.formatter);
	}

	public static String getNewTimeCookie(final HttpExchange http) {
		final LocalDateTime date = LocalDateTime.now().plusMinutes(5);
		final Date expdate = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
		return CookieUtils.df.format(expdate);
	}

}
