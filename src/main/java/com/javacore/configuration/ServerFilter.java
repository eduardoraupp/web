package com.javacore.configuration;

import java.io.IOException;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

public class ServerFilter extends Filter {

	@Override
	public String description() {
		return null;
	}

	@Override
	public void doFilter(final HttpExchange http, final Chain chain) throws IOException {
		http.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		http.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		http.getResponseHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authentication, Authorization, session, username, roles");
		http.getResponseHeaders().add("Access-Control-Expose-Headers", "roles, username, session, next-login");

		http.getRequestHeaders().getFirst("Authorization");
		if (http.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
			http.sendResponseHeaders(200, -1);
		} else {
			chain.doFilter(http);
		}
	}

}
