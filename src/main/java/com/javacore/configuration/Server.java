package com.javacore.configuration;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.javacore.domain.user.User;
import com.javacore.domain.user.UserService;
import com.javacore.infrastructure.http.FrontController;
import com.javacore.infrastructure.http.SecurityRoleFinder;
import com.javacore.infrastructure.interfaces.IUserService;
import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

public class Server {

	public static void main(final String[] args) throws Exception {
		Server.startServer();
	}

	public static HttpServer startServer() throws IOException {
		System.out.println("Starting Server.");
		final IUserService userService = new UserService();
		SecurityRoleFinder.find("com.javacore");
		final FrontController frontController = new FrontController();
		final HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
		final HttpContext ctx = server.createContext("/login", frontController);
		ctx.getFilters().add(new ServerFilter());
		ctx.setAuthenticator(new BasicAuthenticator("login") {
			@Override
			public boolean checkCredentials(final String username, final String password) {
				return userService.getUserWithRolesByUsernameAndPassword(new User(username, password)) != null;
			}
		});
		server.createContext("/users", frontController).getFilters().add(new ServerFilter());
		server.createContext("/page1", frontController).getFilters().add(new ServerFilter());
		server.createContext("/page2", frontController).getFilters().add(new ServerFilter());
		server.createContext("/page3", frontController).getFilters().add(new ServerFilter());
		server.createContext("/admin", frontController).getFilters().add(new ServerFilter());
		final ExecutorService executor = Executors.newCachedThreadPool();
		server.setExecutor(executor);
		server.start();
		System.out.println("Server started on port 8080 and address localhost.");
		return server;
	}

}
