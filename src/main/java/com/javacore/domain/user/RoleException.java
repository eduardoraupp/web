package com.javacore.domain.user;

public class RoleException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public RoleException(final String msg) {
		super(msg);
	}

	public RoleException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

}
