package com.javacore.domain.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class User implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Expose
	private Long id;
	@Expose
	private String username;
	@Expose
	private String password;
	@Expose
	private List<Role> roles;

	public User() {};

	public User(final long id, final String username, final String password, final List<Role> roles) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public User(final long id, final String username, final String password) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
	}

	public User(final String username, final String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public User(final long id, final String username) {
		super();
		this.id = id;
		this.username = username;
	}

	public long getId() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(final List<Role> roles) {
		this.roles = roles;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public void addRole(final Role role) {
		if(this.roles == null) {
			this.roles = new ArrayList<>();
		}
		this.roles.add(role);
	}


}
