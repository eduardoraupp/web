package com.javacore.domain.user;

import java.util.List;

import com.javacore.infrastructure.interfaces.IUserService;
import com.javacore.infrastructure.repositories.UserRepository;

public class UserService implements IUserService {

	private final UserRepository userRepository = new UserRepository();

	public List<User> getUsers() {
		return this.userRepository.getUsers();
	}

	@Override
	public String getRolesByUsername(final String username) {
		return this.userRepository.getRolesByUsername(username);
	}

	@Override
	public User getUserWithRolesByUsernameAndPassword(final User user) {
		return this.userRepository.getUserWithRolesByUsernameAndPassword(user);
	}

	@Override
	public void addUser(final User user) {
		this.userRepository.addUser(user);
	}

	@Override
	public void updateUser(final User user) {
		this.userRepository.updateUser(user);
	}

	@Override
	public void removeUser(final long idUser) {
		this.userRepository.removeUser(idUser);
	}

}
