package com.javacore.domain.user;


public class UserException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public UserException(final String msg){
		super(msg);
	}

	public UserException(final String msg, final Throwable cause){
		super(msg, cause);
	}

}

