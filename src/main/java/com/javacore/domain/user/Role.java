package com.javacore.domain.user;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class Role implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Expose
	private Long id;
	@Expose
	private String role;

	public Role(final String role) {
		this.role = role;
	}

	public Role(final long id, final String role) {
		super();
		this.id = id;
		this.role = role;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(final String role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this.id ^ this.id >>> 32);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Role other = (Role) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Role [id=" + this.id + ", role=" + this.role + "]";
	}



}
