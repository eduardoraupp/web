package com.javacore.domain.security;

import java.util.Arrays;
import java.util.List;

import com.javacore.infrastructure.http.HTTPMethod;

public class EndPointPath {

	private String url;
	private String method;
	private String clazz;
	private int codeReturn;
	private Class<?>[] parameters;
	private List<String> roles;
	private HTTPMethod httpMethod = HTTPMethod.GET;
	private String returnType;

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getMethod() {
		return this.method;
	}

	public void setMethod(final String method) {
		this.method = method;
	}

	public List<String> getRoles() {
		return this.roles;
	}

	public void setRoles(final List<String> roles) {
		this.roles = roles;
	}

	public String getClazz() {
		return this.clazz;
	}

	public void setClazz(final String clazz) {
		this.clazz = clazz;
	}

	public Class<?>[] getParameters() {
		return this.parameters;
	}

	public void setParameters(final Class<?>[] parameters) {
		this.parameters = parameters;
	}

	public HTTPMethod getHttpMethod() {
		return this.httpMethod;
	}

	public void setHttpMethod(final HTTPMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public int getCodeReturn() {
		return this.codeReturn;
	}

	public void setCodeReturn(final int codeReturn) {
		this.codeReturn = codeReturn;
	}

	public boolean requestContainsBody() {
		return this.getHttpMethod().getMethod().equals(HTTPMethod.POST.getMethod())
				|| this.getHttpMethod().getMethod().equals(HTTPMethod.PUT.getMethod());
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(final String returnType) {
		this.returnType = returnType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.httpMethod == null ? 0 : this.httpMethod.hashCode());
		result = prime * result + (this.url == null ? 0 : this.url.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final EndPointPath other = (EndPointPath) obj;
		if (this.httpMethod != other.httpMethod) {
			return false;
		}
		if (this.url == null) {
			if (other.url != null) {
				return false;
			}
		} else if (!this.url.equals(other.url)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "EndPointPath [url=" + this.url + ", method=" + this.method + ", clazz=" + this.clazz + ", codeReturn=" + this.codeReturn
				+ ", parameters=" + Arrays.toString(this.parameters) + ", roles=" + this.roles + ", httpMethod=" + this.httpMethod
				+ ", returnType=" + this.returnType + "]";
	}

}
