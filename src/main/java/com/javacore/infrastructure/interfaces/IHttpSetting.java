package com.javacore.infrastructure.interfaces;

import com.javacore.domain.security.EndPointPath;
import com.sun.net.httpserver.HttpExchange;

public interface IHttpSetting {

	public void setHttpHeadersDefault(HttpExchange http, EndPointPath endPointPath);

	public void setHttpHeadersAfterLogin(HttpExchange http);

	public void nextPageAfterFailed(int status, final HttpExchange http);

	public void setBody(final EndPointPath endPointPath, final Object response, final HttpExchange http);

	public void setCookieExpired(HttpExchange http);

	public void setBodyErrors(final int status, final HttpExchange http, final String message);

}
