package com.javacore.infrastructure.interfaces;

import java.util.List;

import com.javacore.domain.user.User;

public interface IUserService {

	public String getRolesByUsername(final String username);

	public User getUserWithRolesByUsernameAndPassword(final User user);

	public void addUser(User user);

	public void removeUser(final long idUser);

	public void updateUser(final User user);

	public List<User> getUsers();

}
