package com.javacore.infrastructure.http;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javacore.domain.security.EndPointPath;
import com.javacore.infrastructure.interfaces.IHttpSetting;
import com.javacore.utils.CookieUtils;
import com.javacore.utils.StringUtils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class FrontController implements HttpHandler {

	private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	private final IHttpSetting httpSetting = new HttpSetting();

	@Override
	public void handle(final HttpExchange http) throws IOException {
		try {
			final String[] path = http.getHttpContext().getPath().trim().split("/");
			if ("/".concat(path[1]).equals("/login")) {
				this.login(http);
			} else {
				this.handleBodyResponse(http, path);
			}
		} catch (final IOException | SecurityException e) {
			this.httpSetting.setBodyErrors(500, http, e.getMessage());
		}
	}

	private void handleBodyResponse(final HttpExchange http, final String[] path) throws IOException {
		try {
			final LocalDateTime now = LocalDateTime.now();
			final LocalDateTime cookieDate = CookieUtils.getCookieDate(http);
			if (now.isAfter(cookieDate)) {
				this.httpSetting.setCookieExpired(http);
			} else {
				final EndPointPath endPointPath = SecurityRoleFinder.getEndPointPath(path[1], http.getRequestMethod());
				if(this.verifyUserHasRequiredRoles(http, endPointPath)) {
					this.prepareResponse(http, endPointPath);
				}
			}
		} catch (final CookieException e) {
			this.httpSetting.setCookieExpired(http);
		} catch (final IOException e) {
			throw e;
		}
	}

	private void login(final HttpExchange http) throws IOException {
		this.httpSetting.setHttpHeadersAfterLogin(http);
	}

	private boolean verifyUserHasRequiredRoles(final HttpExchange http, final EndPointPath endPointPath)
			throws IOException {
		if (http.getRequestHeaders().getFirst("roles") == null || !StringUtils.verifySomeEqualsGroup(http.getRequestHeaders().getFirst("roles"), endPointPath.getRoles())) {
			this.httpSetting.nextPageAfterFailed(401, http);
			return false;
		}
		return true;
	}


	private void prepareResponse(final HttpExchange http, final EndPointPath endPointPath) throws IOException {
		try {
			final Object[] parameters = StringUtils.getPathParameters(http.getRequestURI().getPath().trim().split("/"));
			final Class<?> clazz = Class.forName(endPointPath.getClazz());
			final Object object = clazz.newInstance();
			final Method method = object.getClass().getMethod(endPointPath.getMethod(), endPointPath.getParameters());
			Object response = null;
			if (endPointPath.requestContainsBody()) {
				response = method.invoke(object, this.gson.fromJson(
						StringUtils.getStringFromInputStream(http.getRequestBody()), endPointPath.getParameters()[0]));
			} else {
				response = method.invoke(object, parameters);
			}
			this.setSucessfulResponse(http, response, endPointPath);
		} catch (final IOException | ClassNotFoundException e) {
			this.httpSetting.setBodyErrors(500, http, e.getMessage());
		} catch (IllegalArgumentException | InstantiationException | IllegalAccessException | NoSuchMethodException
				| InvocationTargetException e) {
			if (e.getCause() instanceof IllegalArgumentException) {
				this.httpSetting.setBodyErrors(400, http, e.getCause().getMessage());
			} else {
				this.httpSetting.setBodyErrors(500, http, e.getCause().getMessage());
			}
		}
	}

	private void setSucessfulResponse(final HttpExchange http, final Object response, final EndPointPath endPointPath)
			throws IOException {
		this.httpSetting.setHttpHeadersDefault(http, endPointPath);
		this.httpSetting.setBody(endPointPath, response, http);
	}

}
