package com.javacore.infrastructure.http;

import java.io.IOException;
import java.io.OutputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javacore.domain.security.EndPointPath;
import com.javacore.domain.user.UserService;
import com.javacore.infrastructure.interfaces.IHttpSetting;
import com.javacore.infrastructure.interfaces.IUserService;
import com.javacore.utils.CookieUtils;
import com.sun.net.httpserver.HttpExchange;

public class HttpSetting implements IHttpSetting {

	private final IUserService userService = new UserService();
	private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

	@Override
	public void setHttpHeadersDefault(final HttpExchange http, final EndPointPath endPointPath) {
		this.setHeaders(http, endPointPath, false);
	}

	@Override
	public void nextPageAfterFailed(final int status, final HttpExchange http) {
		try {
			http.getResponseHeaders().add("next-login", "" + http.getHttpContext().getPath());
			http.getResponseHeaders().set("Content-Type", "text/plain");
			http.sendResponseHeaders(status, -1L);
		} catch (final IOException e) {
			throw new IllegalStateException("Failed in handling the response body.", e.getCause());
		}
	}

	@Override
	public void setHttpHeadersAfterLogin(final HttpExchange http) {
		try {
			final String response = "{}";
			this.setHeaders(http, null, true);
			http.sendResponseHeaders(200, response.getBytes().length);
			final OutputStream os = http.getResponseBody();
			os.write(response.getBytes());
		} catch (final IOException e) {
			throw new IllegalStateException("Failed in handling the response body.", e.getCause());
		} finally {
			try {
				http.getResponseBody().close();
			} catch (final IOException e) {
				throw new IllegalStateException("Failed in closing the response body.", e.getCause());
			}
		}
	}

	private void setHeaders(final HttpExchange http, final EndPointPath endPointPath, final boolean isLogin) {
		if (isLogin) {
			http.getResponseHeaders().set("username", http.getPrincipal().getUsername());
			http.getResponseHeaders().set("roles",
					this.userService.getRolesByUsername(http.getPrincipal().getUsername()));
			http.getResponseHeaders().set("session", CookieUtils.getNewTimeCookie(http));
			http.getResponseHeaders().set("Content-Type", "application/json");
		} else {
			http.getResponseHeaders().set("username", http.getRequestHeaders().getFirst("username"));
			http.getResponseHeaders().set("session", CookieUtils.getNewTimeCookie(http));
			http.getResponseHeaders().set("roles", http.getRequestHeaders().getFirst("roles"));
			http.getResponseHeaders().set("Content-Type", endPointPath.getReturnType());
		}
	}

	@Override
	public void setBody(final EndPointPath endPointPath, final Object response, final HttpExchange http) {
		try {
			if(response != null) {
				final String responseBody = endPointPath.getReturnType().contains("text") ? (String) response
						: this.gson.toJson(response);
				http.sendResponseHeaders(endPointPath.getCodeReturn(), responseBody.getBytes().length);
				final OutputStream os = http.getResponseBody();
				os.write(responseBody.getBytes());
			} else {
				http.sendResponseHeaders(endPointPath.getCodeReturn(), 0L);
			}
		} catch (final IOException e) {
			throw new IllegalStateException("Failed in handling the response body.", e.getCause());
		} finally {
			try {
				http.getResponseBody().close();
			} catch (final IOException e) {
				throw new IllegalStateException("Failed in closing the response body.", e.getCause());
			}
		}
	}

	@Override
	public void setBodyErrors(final int status, final HttpExchange http, final String message) {
		try {
			final String messageResponse = message != null ? message : "Contact the administrator";
			http.sendResponseHeaders(status, messageResponse.getBytes().length);
			final OutputStream os = http.getResponseBody();
			os.write(message.getBytes());
		} catch (final IOException e) {
			throw new IllegalStateException("Failed in handling the response body.", e.getCause());
		} finally {
			try {
				http.getResponseBody().close();
			} catch (final IOException e) {
				throw new IllegalStateException("Failed in closing the response body.", e.getCause());
			}
		}
	}

	@Override
	public void setCookieExpired(final HttpExchange http) {
		try {
			final String response = "Cookie expired";
			this.nextPageAfterFailed(403, http);
			final OutputStream os = http.getResponseBody();
			os.write(response.getBytes());
		} catch (final IOException e) {
			throw new IllegalStateException("Failed in handling the response body.", e.getCause());
		} finally {
			try {
				http.getResponseBody().close();
			} catch (final IOException e) {
				throw new IllegalStateException("Failed in closing the response body.", e.getCause());
			}
		}
	}

}
