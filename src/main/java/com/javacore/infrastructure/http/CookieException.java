package com.javacore.infrastructure.http;

public class CookieException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public CookieException(final String msg){
		super(msg);
	}

	public CookieException(final String msg, final Throwable cause){
		super(msg, cause);
	}

}
