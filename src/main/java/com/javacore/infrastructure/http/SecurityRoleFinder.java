package com.javacore.infrastructure.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;

import com.javacore.domain.security.EndPointPath;
import com.javacore.utils.StringUtils;

public class SecurityRoleFinder {

	private static final char PKG_SEPARATOR = '.';

	private static final char DIR_SEPARATOR = '/';

	private static final String CLASS_FILE_SUFFIX = ".class";

	private static final String BAD_PACKAGE_ERROR = "Unable to get resources from path '%s'. Are you sure the package '%s' exists?";

	private static Set<EndPointPath> endPoints = new HashSet<>();

	public static void find(final String scannedPackage) throws FileNotFoundException {
		try {
			final File fileJar = new File("test-web-application.jar");
			if(fileJar.exists()) {
				SecurityRoleFinder.readJar(fileJar);
			} else {
				final String scannedPath = scannedPackage.replace(SecurityRoleFinder.PKG_SEPARATOR,
						SecurityRoleFinder.DIR_SEPARATOR);
				final URL scannedUrl = Thread.currentThread().getContextClassLoader().getResource(scannedPath);
				SecurityRoleFinder.validatePackage(scannedUrl, scannedPackage, scannedPath);
				final File scannedDir = new File(scannedUrl.getFile().replace("test-classes", "classes"));
				final List<Class<?>> classes = Arrays.asList(scannedDir.listFiles()).stream()
						.flatMap(file -> SecurityRoleFinder.find(file, scannedPackage).stream()).map(file -> file)
						.collect(Collectors.toList());
				SecurityRoleFinder.addEndPointsPaths(classes);
			}
		} catch (final Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private static void readJar(final File file) throws IOException {
		System.out.println("Initializating class annotation loading");
		final FileInputStream fis = new FileInputStream(file);
		final JarInputStream jis = new JarInputStream(fis);
		try {
			JarEntry next = jis.getNextJarEntry();
			final List<Class<?>> classes = new ArrayList<Class<?>>();
			while (next != null) {
				final String name = next.getName();
				if (name.contains("com/javacore/") && name.contains("class")) {
					final String clazz = name.replace("/", ".").replace(".class", "");
					classes.add(Class.forName(clazz));
				}
				next = jis.getNextJarEntry();
			}
			SecurityRoleFinder.addEndPointsPaths(classes);
		} catch (final ClassNotFoundException e) {
			throw new IllegalStateException(e);
		} finally {
			jis.close();
		}
	}

	private static void addEndPointsPaths(final List<Class<?>> classes) {
		for (final Class<?> clazz : classes) {
			final Method[] declaredMethods = clazz.getDeclaredMethods();
			for (final Method method : declaredMethods) {
				if (method.isAnnotationPresent(Address.class) && method.isAnnotationPresent(Security.class)) {
					final EndPointPath ex = new EndPointPath();
					ex.setMethod(method.getName());
					ex.setRoles(Arrays.asList(method.getAnnotation(Security.class).role()).stream()
							.map(role -> role.trim()).collect(Collectors.toList()));
					ex.setUrl(StringUtils.cleanPathResource(method.getAnnotation(Address.class).address()));
					ex.setClazz(clazz.getName());
					ex.setHttpMethod(method.getAnnotation(Address.class).method());
					ex.setParameters(method.getParameterTypes());
					ex.setCodeReturn(method.getAnnotation(Address.class).codeReturn());
					ex.setReturnType(method.getAnnotation(Address.class).returnType());
					SecurityRoleFinder.endPoints.add(ex);
				}
			}
		}
	}

	private static void validatePackage(final URL scannedUrl, final String scannedPackage, final String scannedPath) {
		if (scannedUrl == null) {
			throw new IllegalArgumentException(
					String.format(SecurityRoleFinder.BAD_PACKAGE_ERROR, scannedPath, scannedPackage));
		}
	}

	private static List<Class<?>> find(final File file, final String scannedPackage) {
		final List<Class<?>> classes = new ArrayList<Class<?>>();
		final String resource = scannedPackage + SecurityRoleFinder.PKG_SEPARATOR + file.getName();
		if (file.isDirectory()) {
			for (final File child : file.listFiles()) {
				classes.addAll(SecurityRoleFinder.find(child, resource));
			}
		} else if (resource.endsWith(SecurityRoleFinder.CLASS_FILE_SUFFIX)) {
			final int endIndex = resource.length() - SecurityRoleFinder.CLASS_FILE_SUFFIX.length();
			final String className = resource.substring(0, endIndex);
			try {
				classes.add(Class.forName(className));
			} catch (final ClassNotFoundException ignore) {
			}
		}
		return classes;
	}

	public static EndPointPath getEndPointPath(final String path, final String verb) {
		return SecurityRoleFinder.endPoints.stream()
				.filter(ex -> ex.getUrl().equals(path) && ex.getHttpMethod().getMethod().equalsIgnoreCase(verb))
				.findFirst().orElseThrow(() -> new PathNotValidException("URI not valid."));
	}

}
