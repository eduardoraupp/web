package com.javacore.infrastructure.http;

public enum HTTPMethod {

	POST("POST"), GET("GET"), DELETE("DELETE"), PUT("PUT");

	private String method;

	HTTPMethod(final String method) {
		this.method = method;
	}

	public String getMethod() {
		return this.method;
	}

	public static HTTPMethod getHttpMethod(final String method) {
		return HTTPMethod.valueOf(method);
	}

}
