package com.javacore.infrastructure.http;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Address {

	public String address() default "";
	public int codeReturn() default 200;
	public String returnType() default "application/json";
	public HTTPMethod method() default HTTPMethod.GET;

}

