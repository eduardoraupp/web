package com.javacore.infrastructure.http;

public class PathNotValidException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public PathNotValidException(final String msg){
		super(msg);
	}

	public PathNotValidException(final String msg, final Throwable cause){
		super(msg, cause);
	}

}
