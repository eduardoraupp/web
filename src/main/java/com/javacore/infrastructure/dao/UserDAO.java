package com.javacore.infrastructure.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.javacore.domain.user.Role;
import com.javacore.domain.user.User;
import com.javacore.domain.user.UserException;
import com.javacore.infrastructure.configuration.ConnectionFactory;
import com.javacore.utils.SQLUtils;

public class UserDAO {

	public User getUserByUsernameAndPassword(final User user) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			final Connection connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("SELECT u.id, u.username, u.password FROM user u WHERE username = ? and password = ?");
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getPassword());
			rs = stmt.executeQuery();
			if(rs.next()) {
				return new User(rs.getLong(1), rs.getString(2), rs.getString(3));
			}
			throw new UserException(String.format("User %s NOT FOUND.", user.getUsername()));
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeQueryIODB(rs, stmt);
		}
	}

	public List<User> getUsers() {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			final Connection connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("SELECT u.id, u.username, u.password FROM user u");
			rs = stmt.executeQuery();
			final List<User> users = new ArrayList<>();
			while(rs.next()) {
				users.add(new User(rs.getLong(1), rs.getString(2), rs.getString(3)));
			}
			return users;
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeQueryIODB(rs, stmt);
		}
	}

	public User getUserByUsername(final String username) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			final Connection connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("SELECT id, username, password FROM user WHERE username = ?");
			stmt.setString(1, username);
			rs = stmt.executeQuery();
			if(rs.next()) {
				return new User(rs.getLong(1), rs.getString(2), rs.getString(3));
			}
			throw new UserException(String.format("User %s NOT FOUND.", username));
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeQueryIODB(rs, stmt);
		}
	}

	public User getUserById(final long idUser) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			final Connection connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("SELECT id, username, password FROM user WHERE id = ?");
			stmt.setLong(1, idUser);
			rs = stmt.executeQuery();
			if(rs.next()) {
				return new User(rs.getLong(1), rs.getString(2), rs.getString(3));
			}
			throw new UserException(String.format("User with id %s NOT FOUND.", idUser));
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeQueryIODB(rs, stmt);
		}
	}

	public void save(final User user) {
		PreparedStatement stmt = null;
		Connection connection = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("INSERT INTO user(username, password) VALUES (?, ?)");
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getPassword());
			stmt.executeUpdate();
			final User userCreated = this.getUserByUsername(user.getUsername());
			userCreated.setRoles(user.getRoles());
			this.addUserRoles(userCreated);
		} catch (final SQLException e) {
			try {
				connection.rollback();
			} catch (final SQLException e1) {
				throw new IllegalStateException(e1);
			}
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeSessionDB(connection, stmt);
		}
	}

	public void update(final User user) {
		PreparedStatement stmt = null;
		Connection connection = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("UPDATE user SET username = ?, password = ? WHERE id = ?");
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getPassword());
			stmt.setLong(3, user.getId());
			stmt.executeUpdate();
			this.removeUserRoles(user.getId());
			this.addUserRoles(user);
		} catch (final SQLException e) {
			try {
				connection.rollback();
			} catch (final SQLException e1) {
				throw new IllegalStateException(e1);
			}
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeSessionDB(connection, stmt);
		}
	}

	private void addUserRoles(final User user) {
		PreparedStatement stmt = null;
		Connection connection = null;
		try {
			connection = ConnectionFactory.getConnection();
			for(final Role role : user.getRoles()) {
				stmt = connection.prepareStatement("INSERT INTO user_role(id_user, id_role) VALUES (?, ?)");
				stmt.setLong(1, user.getId());
				stmt.setLong(2, role.getId());
				stmt.executeUpdate();
			}
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeSessionDB(connection, stmt);
		}
	}

	public void removeUser(final long idUser) {
		PreparedStatement stmt = null;
		Connection connection = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("DELETE FROM user WHERE id = ?");
			stmt.setLong(1, idUser);
			stmt.executeUpdate();
			this.removeUserRoles(idUser);
		} catch (final SQLException e) {
			try {
				connection.rollback();
			} catch (final SQLException e1) {
				throw new IllegalStateException(e1);
			}
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeSessionDB(connection, stmt);
		}
	}

	private void removeUserRoles(final long idUser) {
		PreparedStatement stmt = null;
		try {
			final Connection connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("DELETE FROM user_role WHERE id_user = ?");
			stmt.setLong(1, idUser);
			stmt.executeUpdate();
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			try {
				stmt.close();
			} catch (final SQLException e1) {
				throw new IllegalStateException(e1);
			}
		}
	}

}
