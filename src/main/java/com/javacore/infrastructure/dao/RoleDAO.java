package com.javacore.infrastructure.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.javacore.domain.user.Role;
import com.javacore.domain.user.RoleException;
import com.javacore.infrastructure.configuration.ConnectionFactory;
import com.javacore.utils.SQLUtils;

public class RoleDAO {

	public List<Role> getRolesByIdUser(final long idRole) {
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement(
					"SELECT r.id, r.role FROM role r JOIN user_role ur ON ur.id_role = r.id WHERE ur.id_user = ?");
			stmt.setLong(1, idRole);
			rs = stmt.executeQuery();
			final List<Role> roles = new ArrayList<>();
			while(rs.next()) {
				roles.add(new Role(rs.getLong(1), rs.getString(2)));
			}
			if(roles.isEmpty()) {
				throw new RoleException(String.format("Role with id %s NOT FOUND.", idRole));
			}
			return roles;
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeQueryIODB(rs, stmt);
		}
	}

	public Role getRoleByName(final String name) {
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("SELECT id, role FROM role WHERE role = ?");
			stmt.setString(1, name);
			rs = stmt.executeQuery();
			new ArrayList<>();
			while(rs.next()) {
				return new Role(rs.getLong(1), rs.getString(2));
			}
			throw new RoleException(String.format("Role %s NOT FOUND.", name));
		} catch (final SQLException e) {
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeQueryIODB(rs, stmt);
		}
	}

	public void save(final Role role) {
		PreparedStatement stmt = null;
		Connection connection = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("INSERT INTO role(role) VALUES (?)");
			stmt.setString(1, role.getRole());
			stmt.executeUpdate();
		} catch (final SQLException e) {
			try {
				connection.rollback();
			} catch (final SQLException e1) {
				throw new IllegalStateException(e1);
			}
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeSessionDB(connection, stmt);
		}
	}

	public void removeRole(final Role role) {
		PreparedStatement stmt = null;
		Connection connection = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.prepareStatement("DELETE FROM role WHERE id = ?");
			stmt.setLong(1, role.getId());
			stmt.executeUpdate();
		} catch (final SQLException e) {
			try {
				connection.rollback();
			} catch (final SQLException e1) {
				throw new IllegalStateException(e1);
			}
			throw new IllegalStateException(e);
		} finally {
			SQLUtils.closeSessionDB(connection, stmt);
		}
	}

}
