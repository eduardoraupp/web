package com.javacore.infrastructure.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.javacore.domain.user.Role;
import com.javacore.domain.user.User;
import com.javacore.infrastructure.dao.RoleDAO;
import com.javacore.infrastructure.dao.UserDAO;

public class UserRepository {

	private final UserDAO userDAO;
	private final RoleDAO roleDAO;

	public UserRepository() {
		this.userDAO = new UserDAO();
		this.roleDAO = new RoleDAO();
	}

	public List<User> getUsers() {
		return this.userDAO.getUsers();
	}

	public User getUserWithRolesByUsernameAndPassword(final User user) {
		final User userFound = this.userDAO.getUserByUsernameAndPassword(user);
		userFound.setRoles(this.roleDAO.getRolesByIdUser(userFound.getId()));
		return userFound;
	}

	public String getRolesByUsername(final String username) {
		final User userFound = this.userDAO.getUserByUsername(username);
		userFound.setRoles(this.roleDAO.getRolesByIdUser(userFound.getId()));
		return userFound.getRoles().stream().map(role -> role.getRole()).collect(Collectors.joining(", "));
	}

	public void addUser(final User user) {
		if(user.getRoles() == null || user.getRoles().isEmpty()) {
			throw new IllegalArgumentException("Role cannot neither be null nor empty");
		}
		final List<Role> roles = new ArrayList<>();
		user.getRoles().forEach(role -> {
			this.roleDAO.save(role);
			roles.add(this.roleDAO.getRoleByName(role.getRole()));
		});
		user.setRoles(roles);
		this.userDAO.save(user);
	}

	public void removeUser(final long idUser) {
		this.userDAO.removeUser(idUser);
	}

	public void updateUser(final User user) {
		this.userDAO.update(user);
		this.getUserWithRolesByUsernameAndPassword(user);
	}

}
