package com.javacore.infrastructure.configuration;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.h2.tools.RunScript;

public class ConnectionFactory {

	private static Connection conn = null;

	public static Connection getConnection() {
		try {
			if(ConnectionFactory.conn == null || ConnectionFactory.conn.isClosed()) {
				Class.forName("org.h2.Driver");
				ConnectionFactory.conn = DriverManager.getConnection("jdbc:h2:mem:test;", "sa", "sa");
				RunScript.execute(ConnectionFactory.conn, new FileReader("sql.sql"));
			}
			return ConnectionFactory.conn;
		} catch (final SQLException | ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}  catch (final FileNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static void initializeDatabase() {
		try {
			Class.forName("org.h2.Driver");
			final Connection conn = DriverManager.getConnection("jdbc:h2:mem:test;", "sa", "sa");
			RunScript.execute(conn, new FileReader("src/main/resources/sql.sql"));
			conn.prepareStatement("SELECT * FROM USER");
			conn.close();
		} catch (final SQLException | ClassNotFoundException e) {
			throw new IllegalStateException(e);
		} catch (final FileNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
