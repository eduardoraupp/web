package com.javacore.controllers;

import java.util.List;

import com.javacore.domain.user.User;
import com.javacore.domain.user.UserService;
import com.javacore.infrastructure.http.Address;
import com.javacore.infrastructure.http.HTTPMethod;
import com.javacore.infrastructure.http.Security;

public class UserController {

	UserService userService;

	public UserController() {
		this.userService = new UserService();
	}

	@Address(address = "/users", method = HTTPMethod.GET, codeReturn = 200)
	@Security(role ={"PAGE_1", "PAGE_2", "PAGE_3", "ADMIN"})
	public List<User> getUsers() {
		return this.userService.getUsers();
	}

	@Address(address = "/users", method = HTTPMethod.POST, codeReturn = 201)
	@Security(role = "ADMIN")
	public void addUser(final User user) {
		if (user == null) {
			throw new IllegalArgumentException("User cannot be null");
		}
		this.userService.addUser(user);
	}

	@Address(address = "/users/{idUser}", method = HTTPMethod.DELETE, codeReturn = 200)
	@Security(role = "ADMIN")
	public void removeUser(final String idUser) {
		if (idUser == null) {
			throw new IllegalArgumentException("idUser cannot be null");
		}
		this.userService.removeUser(Long.valueOf(idUser));
	}

	@Address(address = "/users", method = HTTPMethod.PUT, codeReturn = 200)
	@Security(role = "ADMIN")
	public void updateUser(final User user) {
		if (user == null) {
			throw new IllegalArgumentException("User cannot be null");
		}
		this.userService.updateUser(user);
	}

}
