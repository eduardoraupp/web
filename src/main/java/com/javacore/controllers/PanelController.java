package com.javacore.controllers;

import com.javacore.infrastructure.http.Address;
import com.javacore.infrastructure.http.Security;

public class PanelController {

	@Address(address="/page1", returnType="text/plain")
	@Security(role={"PAGE_1", "ADMIN"})
	public String getPage1() {
		return "You have permission! Backend sent the message!";
	}

	@Address(address="/page2", returnType="text/plain")
	@Security(role={"PAGE_2", "ADMIN"})
	public String getPage2() {
		return "You have permission! Backend sent the message!";
	}

	@Address(address="/page3", returnType="text/plain")
	@Security(role={"PAGE_3", "ADMIN"})
	public String getPage3() {
		return "You have permission! Backend sent the message!";
	}

}
