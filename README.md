Build the projects with maven - mvn clean compile assembly:single install
	It has unit tests for all the verbs created(http verbs) and front controller tests
	the code can look kind messy but working with reflection cannot be different, to map the endpoints
run the project with: 
	java -jar test-web-application.jar

Build the front end with npm install in the root folder

run the front end inside the root folder by executin grunt serve

access the login form through locahost:9000/login.html 

There are created three users by default:

user page1
pass page1
role PAGE_1

user admin
pass admin
role ADMIN

user page2
pass page2
role PAGE_2

user page3
pass page3
role PAGE_3

If you with to add some new user, you, go to sql.sql and add in there.
Copy the sql.sql in the web source and put in the same folder that the jar exists, it will be read from there.

The post is working, just execut the UserControllerTest, that has some integration tests for DELETE, POST, PUT, GET http verbs.

I did not create a page for add user, just to admin list them.

If you want to add some user, use something such a Postman to do it.
Example:

Body application/json
{"username":"page_test","password":"page_test","roles":[{"role":"ADMIN"}]}

Verb POST

URL localhost:8080/users

headers
session Tue, 17-Apr-2018 00:06:44 CEST
roles ADMIN
Content-Type application/json

FOR PUT IT IS THE SAME, JUST INSERT A VALID USERNAME!

I do not think it was to be something like a CAS, JAAS or even a spring boot security, just understand the flow, but the front controller.

There is no concern about persistence layer, just about decoupling those responsibilities.
There is no concern about the front end, just consume de back end.

There is the endpoints and the front controller, response for controlling the user navigation.

Take a look at IUserService and IHttpSettings, the idea is about injecting them, as the others thing with reflection(Address, Security annotations)
and give to the final developer the possibility to implement UserService as he wishes or the HttpServices, as well.

All the exceptions are thrown by the http, so, if there is any doubt, verify it through postman or other similar
